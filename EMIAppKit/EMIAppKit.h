//
//  EMIAppKit.h
//  EMIAppKit
//
//  Created by Alex on 16/10/15.
//  Copyright © 2015 Alex Steiner. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EMIAppKit.
FOUNDATION_EXPORT double EMIAppKitVersionNumber;

//! Project version string for EMIAppKit.
FOUNDATION_EXPORT const unsigned char EMIAppKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EMIAppKit/PublicHeader.h>


